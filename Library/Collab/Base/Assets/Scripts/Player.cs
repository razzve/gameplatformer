using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    // Dictionary of elements for the UI
    public Dictionary<int, string> elementDict = new Dictionary<int, string>() {{0, "earth"}, {1, "fire"}, {2, "air"}};

    // Switching the element
    public int currElement = 0;

    // How many elements
    public int fire_count = 0;
    public int water_count = 0;
    public int earth_count = 0;
    public int air_count = 0;
    public int bulletForce = 800;
    // Player movement
    public int speed = 4;
    float xSpeed = 0;
    public int jumpForce = 50;
    public GameObject bulletPrefab;
    // Determine if on the ground
    public LayerMask GroundLayer;
    public Transform feetPos;
    public bool grounded = false;

    // Player rigidbody
    Rigidbody2D _rigidbody;

    // Platforms for earth power
    public GameObject groundTarget;
    public GameObject toShake;
    public GameObject toPush;
    Renderer rend;
    Platform plat;

    //Raycasting
    RaycastHit2D hit;
    AudioSource _audioSource;

    
    //center of object
    GameObject _center;


    //animation
    Animator _animator;
    bool actionState;

    GameObject fireEffect;
    GameObject earthEffect;
    GameObject airEffect;

    //UI
    public Text currElementDisplay;
    public Text earth_count_display;
    public Text fire_count_display;
    public Text air_count_display;
    public Text water_count_display;

    public Image highlight0;
    public Image highlight1;
    public Image highlight2;


    void Start()
    {   
        _audioSource = GetComponent<AudioSource>();
        //animation
        _animator = GetComponent<Animator>();

        fireEffect = transform.GetChild(2).gameObject;
        earthEffect = transform.GetChild(3).gameObject;
        airEffect = transform.GetChild(4).gameObject;

        //center of the character
        _center = transform.GetChild(1).gameObject;

        // Get rb
        _rigidbody = GetComponent<Rigidbody2D>();

    }
    void FixedUpdate(){

        


        // Move
        xSpeed = Input.GetAxis("Horizontal") * speed;
        _rigidbody.velocity = new Vector2(xSpeed, _rigidbody.velocity.y);

        //animation
        _animator.SetFloat("speed", Mathf.Abs(xSpeed));


        // Turn
        if ((xSpeed < 0 && transform.localScale.x > 0) || xSpeed > 0 && transform.localScale.x < 0)
        {
            transform.localScale *= new Vector2(-1, 1);
        }

        // Raycast from camera to cursor
        hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero); 

        // If cursor on a Ground       
        if (hit.collider != null && hit.collider.CompareTag("Earth")) {
            groundTarget = hit.collider.gameObject;
            plat = groundTarget.GetComponent<Platform>();
            plat.pusher = _center.transform.position;
        } 
        else{
            plat = null;
            groundTarget = null; 
        }
        
    }
    void Update()
    {   
        //Set UI
        earth_count_display.text = earth_count.ToString();
        fire_count_display.text = fire_count.ToString();
        air_count_display.text = air_count.ToString();
        if (currElement == 0) {
            highlight0.GetComponent<CanvasGroup>().alpha = 1;
            highlight1.GetComponent<CanvasGroup>().alpha = 0;
            highlight2.GetComponent<CanvasGroup>().alpha = 0;
        }
        else if (currElement == 1) {
            highlight0.GetComponent<CanvasGroup>().alpha = 0;
            highlight1.GetComponent<CanvasGroup>().alpha = 1;
            highlight2.GetComponent<CanvasGroup>().alpha = 0;
        }
        if (currElement == 2) {
            highlight0.GetComponent<CanvasGroup>().alpha = 0;
            highlight1.GetComponent<CanvasGroup>().alpha = 0;
            highlight2.GetComponent<CanvasGroup>().alpha = 1;
        }

        // Jumping
        grounded = Physics2D.OverlapCircle(feetPos.position, 1, GroundLayer);
        if (Input.GetButtonDown("Jump") && grounded){
            _rigidbody.velocity = new Vector2(_rigidbody.velocity.x, 0);
            _rigidbody.AddForce(new Vector2(0, jumpForce));
        }

        //animation
        _animator.SetBool("Grounded", grounded);
        _animator.SetBool("Action", actionState);

        if (currElement == 1 && GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("idle")) {
            airEffect.GetComponent<Renderer>().enabled = false;
            earthEffect.GetComponent<Renderer>().enabled = false;
            fireEffect.GetComponent<Renderer>().enabled = true;
        }
        else if (currElement == 0 && GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("idle")){
            airEffect.GetComponent<Renderer>().enabled = false;
            fireEffect.GetComponent<Renderer>().enabled = false;
            earthEffect.GetComponent<Renderer>().enabled = true;
        }
        else if (currElement == 2 && GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("idle")){
            airEffect.GetComponent<Renderer>().enabled = true;
            fireEffect.GetComponent<Renderer>().enabled = false;
            earthEffect.GetComponent<Renderer>().enabled = false;
        }
        else{
            airEffect.GetComponent<Renderer>().enabled = false;
            fireEffect.GetComponent<Renderer>().enabled = false;
            earthEffect.GetComponent<Renderer>().enabled = false;
        }

        // Changing the element
        if (Input.GetButtonDown("Fire1"))
        {
            currElement = (currElement + 1) % 3;
        }

        // On button press
        if (Input.GetButtonDown("Fire2"))
        {
            if (currElement == 1)
            {
                if (fire_count > 0)
                {
                    //Shoot fire
                    GameObject newBullet = Instantiate(bulletPrefab, _center.transform.position, Quaternion.identity);
                    newBullet.GetComponent<Rigidbody2D> ().AddForce(new Vector2(bulletForce*transform.localScale.x,0));
                    fire_count--;
                }
            }
        }

        // When button held
        if (Input.GetButton("Fire2"))
        {   
            actionState = true;
            // If element is earth, if cursor is on a platform, shakes the platform when button is held
            if (currElement == 0){
                Debug.Log("earth");
                if (plat != null)
                {
                    plat.doTheShake = true;
                    
                }
            }
            else if (currElement ==  1)
            {
                Debug.Log("fire");

                
            }
            else if (currElement ==  2)
            {
                Debug.Log("air");
            }
        }

        // On release of mouse2
        if (Input.GetButtonUp("Fire2"))
        {   
            actionState = false;
            // If element is earth and cursor on platform, push it away from player
            if (currElement == 0) {
                if (plat != null && plat.doTheShake && earth_count > 0)
                {
                    plat.StartCoroutine("StartPush");
                    earth_count--;
                }
            }
            
        }

        //dying
        if (transform.position.y < -500) {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    // Collision with triggers
    private void OnTriggerEnter2D(Collider2D other) {

        // Triggering a shrine
        if (other.CompareTag("Shrine"))
        {
            int newElement = other.gameObject.GetComponent<Shrine>().element;

            Destroy(other.gameObject);

            if (newElement == 0) {
                earth_count++;
            }
             else if (newElement == 1) {
                fire_count++;
            }
             else if (newElement == 2) {
                air_count++;
            }
        }

    }

    
    
}
