using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    public Vector3 pusher;
    public bool doTheShake;
    public bool doThePush;
    bool isShaking = false;
    public bool change;
    Renderer rend;

    
    // Start is called before the first frame update
    void Start()
    {
        change = false;
        rend = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (doTheShake && !isShaking)
        {
            StartCoroutine(Shake());
        }

        if (doThePush)
        {   
            
            Vector2 direction = (transform.position - pusher);
            transform.Translate(direction * 2 * Time.deltaTime);
        }
    }

    void LateUpdate()
    {
    }

    IEnumerator Highlight()
    {
        rend.material.color = Color.red;
        yield return new WaitForSeconds(0.5f);
    }

    IEnumerator Shake()
    {   
        float counter = 0f;
        float speed = 0.1f;

        isShaking = true;
        Vector2 ogPosition = transform.position;
        while (counter < 0.2f)
        {   
            
            counter += Time.deltaTime;
            Vector3 tempPos = transform.position + UnityEngine.Random.insideUnitSphere * speed;
            tempPos.z = transform.position.z;
            transform.position = tempPos;

            yield return null;
        }
        if (doThePush == false){
            transform.position = ogPosition;
        }
        isShaking = false;
        doTheShake = false;
        
        
        
    }

    IEnumerator StartPush()
    {

        yield return new WaitForSeconds(0.2f);

        doThePush = true;

        yield return new WaitForSeconds(0.3f);

        doThePush = false;
    }
}
