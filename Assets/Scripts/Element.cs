using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory
{   

    public string title;
    public Sprite icon;
    public Dictionary<string, int> elements = new Dictionary<string, int>();

    public Inventory(string title, Sprite icon, Dictionary<string, int> elements)
    {
        this.title = title;
        this.icon = Resources.Load<Sprite>("Sprites/Elements/" + title);
    }
}
